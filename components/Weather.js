import React , {useState,useEffect} from 'react'
import axios from 'axios'
import LinearGradient from 'react-native-linear-gradient'
import {StyleSheet, View, Text , ScrollView} from 'react-native'

const Weather = (props) => {
    // useEffect(() => { axios way
    //     fetchWeatherData()
    // }, [])
useEffect(()=>{
    if(props.zipCode)
        fetch(`http://api.openweathermap.org/data/2.5/weather?q=${props.zipCode},th&units=metric&APPID=${apiKey}`)
        .then((response)=>response.json())
        .then((json)=>{
            setforcastInfo({
                city : json.name,
                main : json.weather[0].main,
                description : json.weather[0].description.toUpperCase(),
                temp : json.main.temp
            })
        })
        .catch((err) => {
            console.log(err)
        })
},[props.zipCode])
const apiKey = "9319fd19eb6dcb015169112b79d87b66"
const [forcastInfo, setforcastInfo] = useState({
    city : '-',
    main :'-',
    description :'-',
    temp : 0 ,
})

fetchWeatherData = async () => { // axios way
    const res = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${props.zipCode},th&units=metric&APPID=${apiKey}`)
    setforcastInfo({
        city : res.data.name,
        main : res.data.weather[0].main,
        description : res.data.weather[0].description,
        temp : res.data.main.temp
    })
}

    return(
       <View style={weather.linearGradient}>
           <LinearGradient
          colors={['#0093E9', '#80D0C7']}
          style={weather.linearGradient}
        >
          <View>
            <Text style={{ fontSize : 24 , textAlign : "center" }}>{forcastInfo.city}</Text>
            <Text style={{ fontSize : 24 , textAlign : "center" }}>{forcastInfo.main}</Text>
            <Text style={{ fontSize : 24 , textAlign : "center" }}>{forcastInfo.description}</Text>
            <Text style={{ fontSize : 24 , textAlign : "center" }}>{forcastInfo.temp}</Text>
            
          </View>
        </LinearGradient>
       </View>
    )
}
const weather = StyleSheet.create({
    linearGradient: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        alignSelf : "stretch",
        height : 400
      },
})
export default Weather;